const http = require("http");
const fs = require("fs");

// Create a local server to receive data from
const server = http.createServer((req, res) => {
  console.log("Server Started...");
  console.log({ req: req.url });
  switch (req.url) {
    case "/html":
      fs.readFile("index.html", "utf-8", (err, data) => {
        if (err) {
          res.writeHead(400, { "Content-Type": "application/json" });
          res.end(
            JSON.stringify({
              data: "HTML File not found",
            })
          );
        } else {
          res.writeHead(200, { "Content-Type": "text/html" });
          res.end(data);
        }
      });
      break;

    case "/json":
      fs.readFile("data.json", "utf-8", (err, data) => {
        if (err) {
          res.writeHead(400, { "Content-Type": "application/json" });
          res.end(
            JSON.stringify({
              data: "JSON File not found",
            })
          );
        } else {
          res.writeHead(200, { "Content-Type": "application/json" });
          res.end(JSON.stringify(data));
        }
      });
      break;

    case "/index.js":
      fs.readFile("index.js", "utf-8", (err, data) => {
        if (err) {
          res.writeHead(400, { "Content-Type": "application/json" });
          res.end(
            JSON.stringify({
              data: "JSON File not found",
            })
          );
        } else {
          res.writeHead(200, { "Content-Type": "application/javascript" });
          res.end(data);
        }
      });
      break;
  }

  //   res.writeHead(200, { "Content-Type": "application/json" });
  //   res.end(
  //     JSON.stringify({
  //       data: "Hello World!",
  //     })
  //   )
});

server.listen(3000);
